# CS 351 - Programming Paradigms - F19

PREREQUISITE: CS 200/IT 200, CS 210 or permission of the instructor.

This is an examination of the development of programming languages. The emphasis is on the interaction between classes of languages and their associated programming paradigms. Topics include imperative, functional logic, and object-oriented languages.

## Instructor Information
Dr. Stoney Jackson, Professor of CS &amp; IT
Email: Stoney.Jackson@wne.edu
Office: Herman 207b
Phone: 413-782-1314
Office Hours:
MW 9a-11a
F 10a-11a
Appointments: https://stoneyjackson.youcanbook.me/ 

## Required Materials

* GitLab account
    * You may annonymize, but I'll need to know you and your account name.
* Course site on GitLab: https://gitlab.com/cs351-f19
    * Course materials
    * Submissions
* Access to a computer with
    * Git
    * Java JDK 11
    * Python 3
    * H201 and H103 has what you need if you don't have access to a computer outside of school.


## Grading Categories

%  | Category
---|---------
40 | Assignments
30 | Exams (3 @ 10% each)
15 | Reading Quizzes
15 | Attendance and participation (A&amp;P)


## Grading Scale

The following scale is used to map overall scores to letter grades. The top is the minimum overall percentage needed to earn the letter grade below. Overall scores are rounded to the nearest percent before converting to a letter grade.

Min | Grade
----|------
0   | F
60  | D
67  | D+
70  | C-
73  | C
77  | C+
80  | B-
83  | B
87  | B+
90  | A-
93  | A


## Tentative Schedule

| Week | Date  | M              | Date  | W            | 
|------|-------|----------------|-------|--------------| 
| 1    | 08-26 | Syllabus/Setup | 08-28 | Q0           | 
| 2    | 09-02 | LABOR DAY      | 09-04 | Q1-1a        | 
| 3    | 09-09 | Q2             | 09-11 |              | 
| 4    | 09-16 | Q3V1 A1-A2     | 09-18 |              | 
| 5    | 09-23 | Q3V4           | 09-25 |              | 
| 6    | 09-30 | Q3V5 A3-A4     | 10-02 |              | 
| 7    | 10-07 | E0-3V4         | 10-09 | Q3           | 
| 8    | 10-14 | FALL RECESS    | 10-16 |              | 
| 9    | 10-21 | A5-A6          | 10-23 |              | 
| 10   | 10-28 |                | 10-30 | A7-A8 Q3a    | 
| 11   | 11-04 |                | 11-06 |              | 
| 12   | 11-11 | A9-AA Q4       | 11-13 |              | 
| 13   | 11-18 | E3V5-3a        | 11-20 |              | 
| 14   | 11-25 | AB Q5          | 11-27 | THANKSGIVING | 
| 15   | 12-02 |                | 12-04 | AC           | 
| 16   | 12-09 | FINAL?         | 12-11 | FINAL?       | 


## Mapping from slides to assignments

Slides        | Assignemnts
--------------|------------
0, 1, 1a      | A1
2             | A2
3 through V1  | A3
3 through V4  | A4
3 through V5  | A5
3             | A6, A7, A8, A9
3a            | AA
4             | AB
5             | AC


## Reading Quizzes

* Denoted on the schedule as
    * Qn - Reading quiz for slides-n.pdf
    * Qn-m - Reading quiz for slides-1.pdf through slides-m.pdf
    * Q3Vn - Reading quiz for slides-3.pdf through section on language Vn.
* Due by 9am of the day they are listed in the schedule.
* Complete on Kodiak.
* Retake as much as you like.
* Help provide important feedback to direct classroom activities.
* Late submissions are not accepted and receive a 0.


## Assignments

* Denoted on the schedule as
    * An - Assignment An is due.
    * An-Am - Assignments An through Am are due.
* Due by 9am of the day they are listed in the schedule.
* Push to GitLab: see [Submitting work](submitting-work.md).
* Organized and named so that it is easy to read, review, and grade.

### Assignment late policy
Assignments are due at 9am ET on the date listed in the schedule.
A 5% penalty per day late will be applied for the first 2 days late.
On the 3rd day late, late work will receive no more than 55%.
It will be checked for authenticity and completeness,
but will not receive critical feedback.
Assignments more than 1 week late will receive a 0.
Work not submitted correctly is considered not submitted.
Late work will not be accepted after the final exam
(or the last day of class if there is no final),
and will receive a 0.
For exceptions, see Extenuating Circumstances.

Given

* T :: date-time assignment submitted
* D :: assignment due-date-time
* FINAL :: date-time of the start of the final exam

Then the penalty applied is defined in the table below.

Submitted at time T | Total Penalty
--------------------|--------------
T <= D              | 0%
D < T <= D + 1 day  | -5%
D + 1 day < T <= D + 2 days | -10%
D + 2 days < T <= D + 7 days | -45%
D + 7 days < T<br>
- OR -<br>
FINAL < T | -100%


## Exams

* Denoted on the schedule as
    * E0-3V4 - Exam on slides-0.pdf through slides-3.pdf through language V4
* Taken in class.
* Not necessarily mostly multiple choice; likely longer than in other classes you've had with me.
* Last exam is during the normal final exam time.


## Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies,
you may request special considerations for your circumstance.
Make a request as follows:

1. Complete the Extenuating Circumstance "quiz" on Kodiak.
2. Submit hard copies of evidence to your instructor.

Appeals must be submitted as soon as possible once you are aware of the extenuating circumstances.
Your instructor will determine the acceptability of an appeal,
along with the specific accommodations that will be made.

If you have any problems with the above instructions,
please discuss your situation with your instructor. 


## Standard Policies

### Change Clause

Changes may be made to the syllabus or these policies. Changes are announced in class and recorded in the “Change Log” at the end of this document.

### Learning Disabilities
If you have a documented learning disability that requires special accommodation, please call the Learning Disability Services Office at 413-782-1257.

Note: One or more of your peers may be allowed to record audio from class session as part of an accommodation for personal use only. Therefore, be aware that your participation in class may be recorded.

### Academic Honesty
In this class, cheating is defined as
any activity that undermines the instructor’s ability
to assess the knowledge or ability of an individual student.

For homework
* All the work you submit must be your own.
* Do not copy answers from any source that you did not create without proper, clear citation.
* Do not give someone your work lest you become complicit in cheating.

For exams
* All your answers must be your own.
* Use only sources that the instructor has expressly given permission to use.
* Do not allow someone to cheat from you lest you become complicit in cheating.

Discussion and studying with others is useful and encouraged.
However, it’s incumbent on the participants to know where to draw the line.
If you are in doubt,
discuss what is acceptable with your instructor before engaging in such activities,
otherwise avoid the activity.
If you plan to study with others,
here are some tips to help you avoid cheating:
* Avoid discussing details of your solution with another.
* Avoid working out a solution to a problem as a group; as you will all end up with the same work and answer.
* Instead help each other
    * understand the problem
    * identify and understand key concepts from the text that you think are relevant to solving a problem
    * identify and understand examples from the text that you think are relevant to solving a problem

### Attendance and Participation
Each day, you will receive one of the following scores for attendance and participation (A&P).

* P:	100%	Present when roll was taken and no other score applies
* L:	75%	Late: after roll has been taken but before 15 minutes after class started
* IP: 50%	Insufficient Participation: e.g., not on task, not cooperating with your team, miss 15 or more minutes of class, etc.
* A:	0%	Absent: did not make your presence known to the instructor

When you are absent,
you are responsible for all work due that day, exams/quizzes/etc. proctored that day,
and all materials presented or given out that day.

If you are absent (A) on the day of an exam, quiz, or in-class assignment,
you will receive a 0 for that element.

See extenuating exceptions for exceptions.

### Extenuating Circumstances
If a circumstance beyond your control causes you to violate the above policies, you may request special considerations for your circumstance. Make a request as follows:
Complete the Extenuating Circumstance "quiz" on Kodiak.

Submit hard copies of evidence to your instructor.

Appeals must be submitted as soon as possible once you are aware of the extenuating circumstances. Your instructor will determine the acceptability of an appeal, along with the specific accommodations that will be made.

*If you have any problems with the above instructions, please discuss your situation with your instructor.*

