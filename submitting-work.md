# Submitting Work

## Guidelines

Your score for an assignment may be negatively impacted if you do not follow these guidelines. For small assignments, not following these guidelines may result in a less than passing score. Violating some of these may result in a 0 or a late penalty: for example, submitting work to the wrong dropbox or wrong Kodiak classroom.

* Do submit work to the correct Kodiak DropBox in the correct Kodiak classroom.
* Do test your submission after submitting by downloading your submission from Kodiak and viewing its contents to ensure that what you think you submitted is actually what you submitted.
* Do place proper headers on each file you edit. All files you edit must have a header at the top with the following information:
    * Full name
    * Date
    * Class
    * Assignment
    * File
* Do make sure that the proper headers do not break your code. Put proper headers in comments, etc.
* Do cite your sources. Just as in English, you need to credit ideas and work that are not your own in anything you submit. If you do not cite a source, you are taking credit for and claiming ownership of what you submit. If you are not the owner, and you do not cite the source, then you are plagiarizing and in violation of academic honesty.
* Do format your code correctly and consistently. Any standard format is acceptable. But you must be consistent. Be careful about mixing tabs and spaces, as your code may not appear the same when viewed in a different environment.
* Do not submit extra files. Only files that you modified and files necessary to run your code should be submitted.
* Do submit all files needed to run your code.
* Do organize files appropriately in directories and name files appropriately.
* Do organize and staple hardcopies if hardcopies are required.

## Procedure

Clone your private submissions project, and position the terminal in the new folder.

```
git clone https://gitlab.com/cs351/submissions/YOUR-GITLAB-ACCOUNT submissions
cd submissions
```

Create a folder for the assignment you are submitting work for, and position the terminal in the new folder. Let's assume the assignment is A1-A2 for the rest of this document.

```
mkdir A1-A2
cd A1-A2
```

Work on the assignment in A1-A2 using standard editors, file manager, etc. As you work it is a good idea to regularly checkpoint your work by staging, commiting, and pushing your work to GitLab. Assuming the terminal is still in A1-A2 ...

```
git add .
git commit -m "breif desc. of what you did"
git push
```

When you are ready to submit your work, first stage, commit, and push your work as you did above. Then tag your last commit and push the tag.

```
git tag A1-A2
git push --tags
```

If you want to change something and include it in your submission, first stage, commit, and push your work as you did above. Next delete the assignment tag.

```
git tag -d A1-A2
git push --delete origin A1-A2
```

Now recreate and push the tag.

```
git tag A1-A2
git push --tags
```

WARNING: Changing the submission tag after the deadline may cause you submission to be considered late.
