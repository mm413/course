import java.util.*;
import abcdatalog.ast.*;
import abcdatalog.ast.*;
import abcdatalog.engine.bottomup.sequential.*;

public abstract class Program {

    public Program() { } // dummy constructor

    public static Program parse(Scan scn$, Trace trace$) {
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case CLEAR:
            return Clear.parse(scn$,trace$);
        case LIT:
            return DFRQ.parse(scn$,trace$);
        case PRINT:
            return Print.parse(scn$,trace$);
        default:
            throw new RuntimeException("Program cannot begin with " + t$);
        }
    }

    public static Set<Clause> clauses = new HashSet<Clause>();
    public static SemiNaiveEngine engine = null;

    public abstract void eval();

    public static void engineInit() {
        if (engine == null) {
            engine = new SemiNaiveEngine();
            try {
                engine.init(clauses);
            } catch (Exception e) {
                System.out.println(e);
                System.exit(1);
            }
        }
    }

    public static void addClause(Clause clause) {
        clauses.add(clause);
        engine = null; // must do this for any changes in the clauses
    }

    public static void delClause(Clause clause) {
        clauses.remove(clause);
        engine = null; // must do this for any changes in the clauses
    }

    public static void delClauses(PositiveAtom h) {
        engineInit();
        Set<PositiveAtom> rs = engine.query(h);
        for (PositiveAtom a : rs) {
            Clause clause = new Clause(a, new ArrayList<Premise>());
            System.out.println("delete: " + clause);
            delClause(clause);
        }
    }


    public static void clearClauses() {
        clauses = new HashSet<Clause> ();
        engine = null;
    }

    public static void doQuery(PositiveAtom h) {
        engineInit();
        Set<PositiveAtom> rs = engine.query(h);
        for (PositiveAtom a : rs) {
            System.out.println(a);
        }
    }

    public String toString() {
        eval();
        return ""; // default
    }

}
