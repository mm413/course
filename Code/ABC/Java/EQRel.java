import java.util.*;
import abcdatalog.ast.*;
//EQRel:import//

// <rel>:EQRel ::= EQ
public class EQRel extends Rel {



    public EQRel() {

    }

    public static EQRel parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<rel>:EQRel", scn$.lno);
        scn$.match(Token.Val.EQ, trace$);
        return new EQRel();
    }

    public Premise getPremise(Term t1, Term t2) {
        return new BinaryUnifier(t1, t2);
    }

}
