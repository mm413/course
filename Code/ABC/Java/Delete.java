import java.util.*;
import abcdatalog.ast.*;
//Delete:import//

// <dfrq>:Delete ::= DEL
public class Delete extends Dfrq {



    public Delete() {

    }

    public static Delete parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<dfrq>:Delete", scn$.lno);
        scn$.match(Token.Val.DEL, trace$);
        return new Delete();
    }

    public void eval(PositiveAtom h) {
        Program.delClauses(h);
        /*
        Clause clause = new Clause(h, new ArrayList<Premise>());
        System.out.println("delete: " + clause);
        Program.delClause(clause);
        */
    }

}
