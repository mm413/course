import java.util.*;
import abcdatalog.ast.*;
//Print:import//

// <program>:Print ::= PRINT
public class Print extends Program {



    public Print() {

    }

    public static Print parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<program>:Print", scn$.lno);
        scn$.match(Token.Val.PRINT, trace$);
        return new Print();
    }

    public void eval() {
        // print the EDB and IDB clauses, in that order
        String ESD = "";
        String ISD = "";
        for (Clause clause : Program.clauses) {
            if (clause.getBody().size() == 0)
                ESD += clause + "\n";
            else
                ISD += clause + "\n";
        }
        System.out.print(ESD);
        System.out.print(ISD);
    }

}
