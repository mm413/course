import java.util.*;
import abcdatalog.ast.*;
//WC:import//

// <vlw>:WC ::= US
public class WC extends Vlw {



    public WC() {

    }

    public static WC parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<vlw>:WC", scn$.lno);
        scn$.match(Token.Val.US, trace$);
        return new WC();
    }

    public Term getTerm() {
        return Variable.createFreshVariable();
    }

}
