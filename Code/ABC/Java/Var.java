import java.util.*;
import abcdatalog.ast.*;
//Var:import//

// <vlw>:Var ::= <VAR>
public class Var extends Vlw {

    public Token var;

    public Var(Token var) {
        this.var = var;
    }

    public static Var parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<vlw>:Var", scn$.lno);
        Token var = scn$.match(Token.Val.VAR, trace$);
        return new Var(var);
    }

    public Term getTerm() {
        return Variable.create(var.toString());
    }

}
