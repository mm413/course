import java.util.*;
import abcdatalog.ast.*;
//NotConj:import//

// <conjunct>:NotConj ::= NOT <head>
public class NotConj extends Conjunct {

    public Head head;

    public NotConj(Head head) {
        this.head = head;
    }

    public static NotConj parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<conjunct>:NotConj", scn$.lno);
        scn$.match(Token.Val.NOT, trace$);
        Head head = Head.parse(scn$, trace$);
        return new NotConj(head);
    }

    public Premise getPremise() {
        return new NegatedAtom(head.eval());
    }

}
