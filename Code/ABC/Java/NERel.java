import java.util.*;
import abcdatalog.ast.*;
//NERel:import//

// <rel>:NERel ::= NE
public class NERel extends Rel {



    public NERel() {

    }

    public static NERel parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<rel>:NERel", scn$.lno);
        scn$.match(Token.Val.NE, trace$);
        return new NERel();
    }

    public Premise getPremise(Term t1, Term t2) {
        return new BinaryDisunifier(t1, t2);
    }

}
