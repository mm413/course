import java.util.*;
import abcdatalog.ast.*;
//HeadConj:import//

// <conjunct>:HeadConj ::= <head>
public class HeadConj extends Conjunct {

    public Head head;

    public HeadConj(Head head) {
        this.head = head;
    }

    public static HeadConj parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<conjunct>:HeadConj", scn$.lno);
        Head head = Head.parse(scn$, trace$);
        return new HeadConj(head);
    }

    public Premise getPremise() {
        return head.eval();
    }

}
