import java.util.*;
import abcdatalog.ast.*;
//Rel:import//

public abstract class Rel {

    public static Rel parse(Scan scn$, Trace trace$) {
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case EQ:
            return EQRel.parse(scn$,trace$);
        case NE:
            return NERel.parse(scn$,trace$);
        default:
            throw new RuntimeException("Rel cannot begin with " + t$);
        }
    }

    public abstract Premise getPremise(Term t1, Term t2);

}
