import java.util.*;
import abcdatalog.ast.*;
//Lit:import//

// <vlw>:Lit ::= <LIT>
public class Lit extends Vlw {

    public Token lit;

    public Lit(Token lit) {
        this.lit = lit;
    }

    public static Lit parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<vlw>:Lit", scn$.lno);
        Token lit = scn$.match(Token.Val.LIT, trace$);
        return new Lit(lit);
    }

    public Term getTerm() {
        return Constant.create(lit.toString());
    }

}
