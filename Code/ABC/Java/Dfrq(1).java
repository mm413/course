import java.util.*;
import abcdatalog.ast.*;
//Dfrq:import//

public abstract class Dfrq {

    public static Dfrq parse(Scan scn$, Trace trace$) {
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case DEL:
            return Delete.parse(scn$,trace$);
        case DOT:
            return Fact.parse(scn$,trace$);
        case QUERY:
            return Query.parse(scn$,trace$);
        case IF:
            return Rule.parse(scn$,trace$);
        default:
            throw new RuntimeException("Dfrq cannot begin with " + t$);
        }
    }

    public abstract void eval(PositiveAtom h);

}
