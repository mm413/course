import java.util.*;
import abcdatalog.ast.*;
//Args:import//

// <args> **= <vlw> +COMMA
public class Args {

    public List<Vlw> vlwList;

    public Args(List<Vlw> vlwList) {
        this.vlwList = vlwList;
    }

    public static Args parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<args>", scn$.lno);
        List<Vlw> vlwList = new ArrayList<Vlw>();
        // first trip through the parse
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case VAR:
        case US:
        case LIT:
            while(true) {
                vlwList.add(Vlw.parse(scn$, trace$));
                t$ = scn$.cur();
                v$ = t$.val;
                if (v$ != Token.Val.COMMA)
                    break; // not a separator, so we're done
                scn$.match(v$, trace$);
            }
        } // end of switch
        return new Args(vlwList);

    }

    public Term [] getTerms() {
        ArrayList<Term> terms = new ArrayList<Term>();
        for (Vlw vlw : vlwList) {
            terms.add(vlw.getTerm());
        }
        int size = terms.size();
        return terms.toArray(new Term[0]);
    }

}
