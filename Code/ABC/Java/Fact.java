import java.util.*;
import abcdatalog.ast.*;
//Fact:import//

// <dfrq>:Fact ::= DOT
public class Fact extends Dfrq {



    public Fact() {

    }

    public static Fact parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<dfrq>:Fact", scn$.lno);
        scn$.match(Token.Val.DOT, trace$);
        return new Fact();
    }

    public void eval(PositiveAtom h) {
        Clause clause = new Clause(h, new ArrayList<Premise>());
        // System.out.println("fact: " + clause);
        Program.addClause(clause);
    }

}
