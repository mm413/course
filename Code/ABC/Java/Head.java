import java.util.*;
import abcdatalog.ast.*;
//Head:import//

// <head> ::= <LIT> LPAREN <args> RPAREN
public class Head {

    public Token lit;
    public Args args;

    public Head(Token lit, Args args) {
        this.lit = lit;
        this.args = args;
    }

    public static Head parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<head>", scn$.lno);
        Token lit = scn$.match(Token.Val.LIT, trace$);
        scn$.match(Token.Val.LPAREN, trace$);
        Args args = Args.parse(scn$, trace$);
        scn$.match(Token.Val.RPAREN, trace$);
        return new Head(lit, args);
    }

    public PositiveAtom eval() {
        String predSym = lit.toString();
        Term [] terms = args.getTerms();
        return PositiveAtom.create(PredicateSym.create(predSym, terms.length), terms);
    }

}
