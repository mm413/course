import java.util.*;
import abcdatalog.ast.*;
//Clear:import//

// <program>:Clear ::= CLEAR
public class Clear extends Program {



    public Clear() {

    }

    public static Clear parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<program>:Clear", scn$.lno);
        scn$.match(Token.Val.CLEAR, trace$);
        return new Clear();
    }

    public void eval() {
        Program.clearClauses();
    }

}
