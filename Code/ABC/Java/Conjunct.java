import java.util.*;
import abcdatalog.ast.*;
//Conjunct:import//

public abstract class Conjunct {

    public static Conjunct parse(Scan scn$, Trace trace$) {
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case LIT:
            return HeadConj.parse(scn$,trace$);
        case NOT:
            return NotConj.parse(scn$,trace$);
        case VAR:
            return RelConj.parse(scn$,trace$);
        default:
            throw new RuntimeException("Conjunct cannot begin with " + t$);
        }
    }

    public abstract Premise getPremise();

}
