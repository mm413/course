import java.util.*;
import abcdatalog.ast.*;
//Rule:import//

// <dfrq>:Rule ::= IF <conjuncts> DOT
public class Rule extends Dfrq {

    public Conjuncts conjuncts;

    public Rule(Conjuncts conjuncts) {
        this.conjuncts = conjuncts;
    }

    public static Rule parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<dfrq>:Rule", scn$.lno);
        scn$.match(Token.Val.IF, trace$);
        Conjuncts conjuncts = Conjuncts.parse(scn$, trace$);
        scn$.match(Token.Val.DOT, trace$);
        return new Rule(conjuncts);
    }

    public void eval(PositiveAtom h) {
        List<Premise> body = new ArrayList<Premise>();
        conjuncts.eval(body);
        Clause clause = new Clause(h, body);
        Program.addClause(clause);
    }

}
