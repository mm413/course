import java.util.*;
import abcdatalog.ast.*;
//Query:import//

// <dfrq>:Query ::= QUERY
public class Query extends Dfrq {



    public Query() {

    }

    public static Query parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<dfrq>:Query", scn$.lno);
        scn$.match(Token.Val.QUERY, trace$);
        return new Query();
    }

    public void eval(PositiveAtom h) {
        Program.doQuery(h);
    }

}
