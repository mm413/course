import java.util.*;
import abcdatalog.ast.*;
//Vlw:import//

public abstract class Vlw {

    public static Vlw parse(Scan scn$, Trace trace$) {
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case US:
            return WC.parse(scn$,trace$);
        case LIT:
            return Lit.parse(scn$,trace$);
        case VAR:
            return Var.parse(scn$,trace$);
        default:
            throw new RuntimeException("Vlw cannot begin with " + t$);
        }
    }

    public abstract Term getTerm();

}
