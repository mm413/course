import java.util.*;
import abcdatalog.ast.*;
//RelConj:import//

// <conjunct>:RelConj ::= <VAR>lh <rel> <vlw>rh
public class RelConj extends Conjunct {

    public Token lh;
    public Rel rel;
    public Vlw rh;

    public RelConj(Token lh, Rel rel, Vlw rh) {
        this.lh = lh;
        this.rel = rel;
        this.rh = rh;
    }

    public static RelConj parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<conjunct>:RelConj", scn$.lno);
        Token lh = scn$.match(Token.Val.VAR, trace$);
        Rel rel = Rel.parse(scn$, trace$);
        Vlw rh = Vlw.parse(scn$, trace$);
        return new RelConj(lh, rel, rh);
    }

    public Premise getPremise() {
        Term t1 = Variable.create(lh.toString());
        Term t2 = rh.getTerm();
        return rel.getPremise(t1, t2);
    }

}
