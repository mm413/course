import java.util.*;
import abcdatalog.ast.*;
//Conjuncts:import//

// <conjuncts> **= <conjunct> +COMMA
public class Conjuncts {

    public List<Conjunct> conjunctList;

    public Conjuncts(List<Conjunct> conjunctList) {
        this.conjunctList = conjunctList;
    }

    public static Conjuncts parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<conjuncts>", scn$.lno);
        List<Conjunct> conjunctList = new ArrayList<Conjunct>();
        // first trip through the parse
        Token t$ = scn$.cur();
        Token.Val v$ = t$.val;
        switch(v$) {
        case VAR:
        case LIT:
        case NOT:
            while(true) {
                conjunctList.add(Conjunct.parse(scn$, trace$));
                t$ = scn$.cur();
                v$ = t$.val;
                if (v$ != Token.Val.COMMA)
                    break; // not a separator, so we're done
                scn$.match(v$, trace$);
            }
        } // end of switch
        return new Conjuncts(conjunctList);

    }

    public void eval(List<Premise> body) {
        for (Conjunct conj : conjunctList) {
            body.add(conj.getPremise());
        }
    }

}
