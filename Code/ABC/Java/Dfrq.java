import java.util.*;
import abcdatalog.ast.*;
//DFRQ:import//

// <program>:DFRQ ::= <head> <dfrq>
public class DFRQ extends Program {

    public Head head;
    public Dfrq dfrq;

    public DFRQ(Head head, Dfrq dfrq) {
        this.head = head;
        this.dfrq = dfrq;
    }

    public static DFRQ parse(Scan scn$, Trace trace$) {
        if (trace$ != null)
            trace$ = trace$.nonterm("<program>:DFRQ", scn$.lno);
        Head head = Head.parse(scn$, trace$);
        Dfrq dfrq = Dfrq.parse(scn$, trace$);
        return new DFRQ(head, dfrq);
    }


    public void eval() {
        PositiveAtom h = head.eval();
        dfrq.eval(h);
    }


}
